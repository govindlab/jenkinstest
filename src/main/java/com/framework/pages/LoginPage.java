package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="username") WebElement eleUsername;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	
	public LoginPage enterUsername(String data) {
		clearAndType(eleUsername, data);
		return this; 
	}
	public LoginPage enterPassword(String data) {
		clearAndType(elePassword, data);
		return this;
	}
	public HomePage clickLogin() {
	    click(eleLogin);
	    return new HomePage();
	}
}













