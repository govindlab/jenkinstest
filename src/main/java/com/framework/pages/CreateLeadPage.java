package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompname;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement btnCreateLead;
	
	public CreateLeadPage enterCompname(String compName) {
		clearAndType(eleCompname, compName);
		return this; 
	}
	
	public CreateLeadPage enterFirstName(String fName) {		
		clearAndType(eleFName, fName);
		return this;
	}
	
	public CreateLeadPage enterLastName(String lName) {		
		clearAndType(eleLName, lName);
		return this;
	}
	
	public ViewLeadPage clickCreateLead() {		
	    click(btnCreateLead);	    
	    return new ViewLeadPage();
	}
}













