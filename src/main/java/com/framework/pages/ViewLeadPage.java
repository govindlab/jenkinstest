package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public String data;
	
	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH,using="//span[@id='viewLead_companyName_sp']") WebElement lblCompName;
	public ViewLeadPage verifyCreatedLead(String nameExpected) {
		
		if(nameExpected.equals("default"))
			nameExpected = data;
		if(verifyPartialText(lblCompName, nameExpected))
		{
			int i = nameExpected.length();			
			logStep("pass", "The Lead is created successfully.The Lead ID is " + getElementText(lblCompName).substring(i+2,i+7));
		}
		else
			logStep("fail", "The Lead is NOT created");
		
		return this; 
	}
	
	@FindBy(how = How.XPATH,using="//span[@id='viewLead_companyName_sp']") WebElement lblCompName2;
	public ViewLeadPage getCompName()
	{
		data = lblCompName2.getText();
		data = data.substring(0,data.length()-8);
		return this;
	}
	
	
	@FindBy(how = How.LINK_TEXT,using="Duplicate Lead") WebElement lnkDuplicateLead;
	public DuplicateLeadPage clickDuplicateLead()
	{
		click(lnkDuplicateLead);
		return new DuplicateLeadPage();
	}


}
















