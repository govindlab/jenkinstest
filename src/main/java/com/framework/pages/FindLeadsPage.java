package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//span[text()='Email']") WebElement lblEmailTab;
	@FindBy(how = How.XPATH,using="//input[@name='emailAddress']") WebElement txtEmail;
	@FindBy(how = How.XPATH,using="//button[text()='Find Leads']") WebElement btnFindLeads;
	@FindBy(how = How.XPATH, using = "//table[@class = 'x-grid3-row-table']//td[contains(@class,'x-grid3-td-partyId')]//a") WebElement lnkFirstLead;
	
	public FindLeadsPage clickEmailTab() {
		click(lblEmailTab);
		return this; 
	}
	
	public FindLeadsPage enterEmailAddress(String eMailID) {		
		clearAndType(txtEmail, eMailID);
		return this;
	}
	
	public FindLeadsPage clickFindLeadsButton() {		
	    click(btnFindLeads);	    
	    return this;
	}
	
	
	public ViewLeadPage clickFirstLead() {		
	    click(lnkFirstLead);	    
	    return new ViewLeadPage();
	}
}













