package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_FindLead extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC002_FindLead";
		testDescription = "Find a Lead";
		author = "Govind";
		testNodes = "Leads";
		category = "Regression";
		dataSheetName = "TC002_FindLead";
	}
	
	@Test(dataProvider="fetchData")
	public void findLead(String userName, String password, String eMailId, String nameExpected)
	{
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.clickEmailTab()
		.enterEmailAddress(eMailId)
		.clickFindLeadsButton()
		.clickFirstLead()
		.getCompName()
		.clickDuplicateLead()
		.clickCreateLead();
		//.verifyCreatedLead(nameExpected);		
	}

}
