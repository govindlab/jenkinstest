package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC001_CreateLead";
		testDescription = "Create a new Lead";
		author = "Govind";
		testNodes = "Leads";
		category = "Regression";
		dataSheetName = "TC001_CreateLead";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String userName, String password, String compName, String fName, String lName, String nameExpected)
	{
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()		
		.clickCreateLead()
		.enterCompname(compName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLead()
		.verifyCreatedLead(nameExpected);
	}
}
